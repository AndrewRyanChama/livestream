import sqlite3
import hashlib
import uuid
import json
import time
import base64
from flask import Flask, g, request, app, redirect, Response, abort, render_template
from werkzeug.wrappers import response

DATABASE = '/home/debian/livestream/database.db'

VID_SALT = 'changeme1'
CHAT_SALT = 'changeme2'
UID_SALT = 'changeme3'

def vid_id(user_id):
    h = hashlib.sha256((user_id + VID_SALT).encode())
    return h.hexdigest()

def vid_scoped_chat_id(user_id, vid_id):
    h = hashlib.sha256((user_id + vid_id + CHAT_SALT).encode())
    return base64.b64encode(h.digest()).decode('ascii')[:7]

app = Flask(__name__)
application = app
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

@app.route('/chat', methods=['GET', 'POST'])
def chat():
    if request.method == 'GET':
        vidid = request.args['v']
        lastSpoken = int(request.args.get('last_spoken', "0"))
        cur = get_db().cursor()
        cur.execute('SELECT * FROM chat WHERE spoken_at > ? AND stream_id = ? ORDER BY spoken_at DESC LIMIT 50', (lastSpoken, vidid))
        res = cur.fetchall()
        return json.dumps([{k: row[k] for k in row.keys()} for row in res])
    else:
        ip = request.remote_addr
        vidid = request.json['v']
        message = request.json['message']
        userid = vid_scoped_chat_id(ip, vidid)
        cur = get_db().cursor()
        cur.execute('INSERT INTO chat (chatter_id, stream_id, message, spoken_at) values (?, ?, ?, ?)', (userid, vidid, message, int(time.time() * 1000)))
        get_db().commit()
        return 'success', 200

@app.route('/manage', methods=['GET','POST'])
def manage():
    ip = request.remote_addr
    vidid = vid_id(ip)
    if request.method == 'GET':
        # fetch the record
        cur = get_db().cursor()
        cur.execute('SELECT * FROM streams WHERE id = ?', (vidid,))
        res = cur.fetchone()
        if res is None:
            streamkey = str(uuid.uuid4())
            cur = get_db().cursor()
            cur.execute('INSERT INTO streams (id, stream_key) values (?, ?)', (vidid, streamkey))
            get_db().commit()
            cur = get_db().cursor()
            cur.execute('SELECT * FROM streams WHERE id = ?', (vidid,))
            res = cur.fetchone()
        return render_template('manage.html', **res)
    else:
        cur = get_db().cursor()
        cur.execute('UPDATE streams SET title = ?, information = ?, author=? WHERE id = ?', (request.form['title'], request.form['information'], request.form['author'], vidid))
        get_db().commit()
        cur = get_db().cursor()
        cur.execute('SELECT * FROM streams WHERE id = ?', (vidid,))
        res = cur.fetchone()
        return render_template('manage.html', **res)

@app.route('/watch')
def watch():
    vidid = request.args['v']
    cur = get_db().cursor()
    cur.execute('SELECT * FROM streams WHERE id = ?', (vidid,))
    res = cur.fetchone()
    return render_template('watch.html', string_content=json.dumps(res['information']), **res)

@app.route('/live_now')
def live_now():
    cur = get_db().cursor()
    cur.execute('SELECT * FROM streams WHERE live_now = 1 order by live_at desc')
    res = cur.fetchall()
    return render_template('live_now.html', vids = res)

@app.route('/on_publish', methods=['POST'])
def on_publish():
    stream_key = request.form.get('name')
    print("started stream", stream_key)
    cur = get_db().cursor()
    cur.execute('SELECT id FROM streams WHERE stream_key = ?', (stream_key,))
    res = cur.fetchone()
    print(res)
    if res is None:
        print("abortingggg")
        abort(404)
    cur = get_db().cursor()
    cur.execute('UPDATE streams SET live_now=1, live_at = ? WHERE stream_key = ?', (int(time.time() * 1000), stream_key))
    get_db().commit()
    vidid = res['id']
    response = Response()
    response.status_code = 301
    response.headers['Location'] = vidid
    response.autocorrect_location_header = False
    return response

@app.route('/on_publish_done', methods=['POST'])
def on_publish_done():
    stream_key = request.form.get('name')
    cur = get_db().cursor()
    cur.execute('UPDATE streams SET live_now=0 WHERE stream_key = ?', (stream_key,))
    get_db().commit()
    print("ended stream", stream_key)
    return ""

if __name__ == "__main__":
    app.run(port=8002)
