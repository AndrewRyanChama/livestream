CREATE TABLE streams (
   id text primary key,
   stream_key text,
   title text,
   information text,
   author text,
   live_now boolean default 0,
   live_at integer default 0,
   unique(stream_key)
);

CREATE TABLE chat (
   chatter_id text,
   stream_id text,
   message text,
   spoken_at integer
);

CREATE INDEX latest_chats ON chat(stream_id, spoken_at);

CREATE TABLE live_viewer (
   viewer_id text,
   stream_id text,
   last_seen integer
);
CREATE INDEX latest_views ON live_viewer(stream_id, last_seen);